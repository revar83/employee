const cors = require('cors')
const employee = require('./employee.json');
const fs = require('fs');
const express = require('express');

const app = express();
app.use(cors());
app.use(express.json())
// Route to get all posts
app.get("/employee", (req,res)=>{
  res.send(employee);
});




// set port, listen for requests
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});