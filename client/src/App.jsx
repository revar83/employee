import React,{useEffect, useState} from "react";
import { EmployeeContextProvider } from "./store/EmployeeContextProvider";
import  EmployeeList  from "./Components/EmployeeList";
import  Search  from "./Components/Search";
import  Mychart  from "./Components/Mychart";

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container'
import "./App.css";

function App() {
  return (
    <div className="App">
      <Container>
      <Row>
      <Col xs={12} md={12}>

          <div className="head col-space">
            <h2>
                Employee Dashboard
            </h2>
          </div>
        </Col>
        </Row>
          <EmployeeContextProvider>
            <Row>
            <Col xs={12} md={ 4 } className="col-space">

              <Search/>
            </Col>
            </Row>
            <Row>
            <Col><EmployeeList/></Col>
              
            </Row> 
            <Row className="col-space">
                <Col xs={12} md={ 6 }>
                    <Mychart 
                      title="Skill Set"
                      plotlabel = {['Javascript','React','Java','C','C++','Python']}
                      label ="Skills"
                      type="skill"
                      chartType ="line"
                      background ="rgba(255, 99, 132, 0.5)"
                      borderCol =  'rgb(255, 99, 132)'
                    />
                </Col>
                <Col xs={12} md={ 6 }>
                  <Mychart 
                      title="Designation"
                      plotlabel = {['Senior Software Engineer II','Senior Software Engineer','Manager','Software Engineer']}
                      label ="Designation"
                      type="designation"
                      chartType ="line"
                      background ="rgba(255, 99, 132, 0.5)"
                      borderCol =  'rgb(255, 99, 132)'

                    />
                </Col>
                
            </Row>
            <Row className="col-space pt-20">
            <Col xs={12} md={ 6 }>
                  <Mychart 
                      title="Salary"
                      plotlabel = {[10000000,2500000,3500000]}
                      label ="Designation"
                      type="salary"
                      chartType ="donut"
                      background ={[
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                      ]}
                      borderCol =  {[
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                      ]}

                    />
                </Col>
            </Row>
            
          </EmployeeContextProvider>
          
      </Container>
    </div>
  );
}

export default App;