import React,{useState, useEffect} from 'react';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    ArcElement,
    Legend,
  } from 'chart.js';
import { Line, Doughnut } from 'react-chartjs-2';
import {PopulateSkill, PopulateDesigData, PopulateSalaryData} from '../../libs/PopulateChart'

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  ArcElement,
  Tooltip,
  Legend
);
  
export const Mychart = (props) => {
    const labels = props.plotlabel;
    const [filterData, setFilterData] = useState();
    const options = {
        responsive: true,
        interaction: {
          mode: 'index',
          intersect: false,
        },
        stacked: false,
        plugins: {
          title: {
            display: true,
            text: props.title,
          },
        },
        scales: {
          y: {
            type: 'linear',
            display: true,
            position: 'left',
            beginAtZero: true
          }
        },
      };
    const getChartData=()=>{
        const labeltype = props.type;
        switch(labeltype){
            case 'skill':
                let Skilldata = PopulateSkill({"labels":props.plotlabel});
                return(Skilldata);
                break;
            case 'designation':
                let desigData = PopulateDesigData({"labels":props.plotlabel});
                return(desigData);
                break;
            case 'salary':
              let salaryData = PopulateSalaryData({"labels":props.plotlabel});
              return(salaryData);
              break;
            default :
                let defaultdata = PopulateSkill({"labels":props.plotlabel});
                return(defaultdata);
                break;
        }
    }
    
    const data = {
        labels,
        datasets: [
          {
            label: props.label,
            data: getChartData(),
            borderColor: props.borderCol,
            backgroundColor: props.background,
            yAxisID: 'y',
          }
        ],
      };
  
      return(props.chartType== 'line'?    
         <Line options={options} data={data} /> 
       : <Doughnut data={data} /> )
    
}
export default Mychart;