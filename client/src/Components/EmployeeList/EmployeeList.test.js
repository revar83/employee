import { render, screen } from '@testing-library/react';
import {EmployeeList} from './EmployeeList';

test('Employee Element', () => {
    
    render(<EmployeeList />);
    //   const linkElement = screen.getByText(/learn react/i);
    //   expect(linkElement).toBeInTheDocument();
    expect(screen.queryByTestId(/employeeList/i)).toBeTruthy();
    expect(screen.queryByText(/Name/i)).toBeTruthy();
    expect(screen.queryByText(/Skillset/i)).toBeTruthy();

});
