import React,{useContext,useState,useEffect} from 'react';
import {EmployeeContext} from '../../store/EmployeeContextProvider';
import Table from 'react-bootstrap/Table';
import CurrencyFormat from 'react-currency-format';

export const EmployeeList = () => {
    const empContext = useContext(EmployeeContext);
    const [empList, setEmpList] = useState(useContext(EmployeeContext));
    
    const getData = (context,args) =>{
        const enteredVal = context.target.value;
        const filteredval = empContext.reduce((filterVal, empdet)=>{
            if((((empdet.name).toLowerCase()).indexOf(enteredVal.toLowerCase()) > -1) || (((empdet.skill).toLowerCase()).indexOf(enteredVal.toLowerCase()) > -1) || (((empdet.designation).toLowerCase()).indexOf(enteredVal.toLowerCase()) > -1)) {
                filterVal.push(empdet);
            }
            return filterVal;
        },[]);
        setEmpList(filteredval);
    }
    const filterEmployee =(d)=>{
        let timer;
        return function(){
            let context = this,
            args = arguments; 
            clearTimeout(timer);
            timer = setTimeout(
                ()=>{
                    getData.apply(context,args);
                },d
            )
        }
    }
    const ele = document.querySelector("#search");
    if(ele){

        ele.addEventListener('keyup', (e) => {
            filterEmployee(3000)(e);
        });
    }
    
    return (
            <Table striped bordered hover data-testid="employeeList">
            <thead>
                <tr>
                <th>#</th>
                <th>Name</th>
                <th>Skillset</th>
                <th>Designation</th>
                <th>Salary</th>
                </tr>
            </thead>
            <tbody>
            {empList?.length?
            empList.map((empdet,key)=>(<tr>
            <td>{empdet.employeeCode}</td>
            <td>{empdet.name}</td>
            <td>{empdet.skill}</td>
            <td>{empdet.designation}</td>
            <td>
                <CurrencyFormat value={empdet.salary} displayType={'text'} thousandSeparator={true} prefix={'$'} />

            </td>
            </tr>))
             : null }
            </tbody>
            </Table>
    )
}