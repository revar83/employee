import React from 'react';

export const Search = () => {
    return (
        <input type="text" name="search" id="search" placeholder='Search Name,Designation,SkillSet' data-testid="search" className="text-style"/>
    )
}
export default Search;
