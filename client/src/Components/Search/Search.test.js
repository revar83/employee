import { render, screen, fireEvent } from '@testing-library/react';
import Search from './Search';
import {EmployeeContext} from '../../store/EmployeeContextProvider';
const employee = require('../../employee.json'); 

test('Search Element', () => {
    render(<Search />);
    const input = screen.queryByTestId(/search/i);

    fireEvent.change(input, { target: { value: 'Revathi' } })
    expect(input).toHaveValue('Revathi')

    expect(employee).toEqual(
        expect.arrayContaining([
          expect.objectContaining({employeeCode: 'IN01'}),
          expect.objectContaining({name: 'Revathi Ramasamy'}),
          expect.objectContaining({name: 'Revathi Ramasamy'}),

        ])
      );

    //   const linkElement = screen.getByText(/learn react/i);
    //   expect(linkElement).toBeInTheDocument();
    expect(screen.queryByTestId(/search/i)).toBeTruthy();

});
