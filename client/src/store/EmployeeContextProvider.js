import React, {  createContext, useEffect, useState } from 'react';
export const EmployeeContext = createContext(null);
export const EmployeeContextProvider = (props) => {
    const [employee, setEmployee] = useState(null);
    useEffect(() => {
        fetch("/employee")
          .then((res) => res.json())
          .then((data) => {            
             setEmployee(data);
          });
          
      }, []);
    return (
        employee?
        <EmployeeContext.Provider value={employee}>
            {props.children}
        </EmployeeContext.Provider>:"No Results found!!"
    )
}
export default EmployeeContextProvider;