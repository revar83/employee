import React,{useContext,useState,useEffect} from 'react';
import {EmployeeContext} from '../store/EmployeeContextProvider';

export const EmployeeData = () =>{
    const empContext = useContext(EmployeeContext);
    return empContext;
}
 
export const PopulateSkill = (args) =>{
    const empData = EmployeeData();
    const labels = args.labels;
    const filteredData = empData.reduce((dataArray, empDet)=>{
        dataArray = labels.reduce((plotData, label)=>{
            const empSkillArr = ((empDet.skill).toLowerCase()).split(',');

            plotData = plotData.length > 0 ? plotData :(dataArray ? dataArray : plotData);
            let Count = plotData[label.toLowerCase()] ? plotData[label.toLowerCase()] :0;
            if(empSkillArr.indexOf(label.toLowerCase()) > -1){
                ++Count;
            }
            plotData[label.toLowerCase()] = Count;
            return plotData;
        },{});
        return dataArray;
    },{});
    const displayData = Object.keys(filteredData).reduce((dispData,key) => {
        dispData.push(filteredData[key]);
        return dispData;
    },[]);
    return(displayData);
}
export const PopulateDesigData = (args) =>{
    const empData = EmployeeData();
    const labels = args.labels;
    const filteredData = empData.reduce((dataArray, empDet)=>{
        dataArray = labels.reduce((plotData, label)=>{
            plotData = plotData.length > 0 ? plotData :(dataArray ? dataArray : plotData);
            let Count = plotData[label.toLowerCase()] ? plotData[label.toLowerCase()] :0;
            if(label.toLowerCase() === (empDet.designation).toLowerCase()){
                ++Count;
            }
            plotData[label.toLowerCase()] = Count;
            return plotData;
        },{});
        return dataArray;
    },{});
    const displayData = Object.keys(filteredData).reduce((dispData,key) => {
        dispData.push(filteredData[key]);
        return dispData;
    },[]);
    return(displayData);
}
export const PopulateSalaryData = (args) =>{
    const empData = EmployeeData();
    const labels = args.labels;
    const filteredData = empData.reduce((dataArray, empDet)=>{
        dataArray = labels.reduce((plotData, label)=>{
            console.log("plot",empDet.salary,plotData,dataArray );
            plotData = plotData.length > 0 ? plotData :(dataArray ? dataArray : plotData);
            let Count = plotData[label] ? plotData[label] :0;
            if(label == (empDet.salary)){
                ++Count;
            }
            plotData[label] = Count;
            return plotData;
        },{});
        return dataArray;
    },{});
    const displayData = Object.keys(filteredData).reduce((dispData,key) => {
        dispData.push(filteredData[key]);
        return dispData;
    },[]);
    return(displayData);
}
export default PopulateSkill;